from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ipv4
from ryu.lib.packet import vlan
from ryu.ofproto.ether import ETH_TYPE_8021Q
from logging import getLogger, DEBUG, Formatter
from logging.handlers import RotatingFileHandler as RFH


class GateWay(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(GateWay, self).__init__(*args, **kwargs)
        logger = getLogger(__name__)
        file_handler = RFH('./log/test_navit.log', 'a+', 100000, 100)
        log_fmt = '%(asctime)s- %(name)s - %(levelname)s - %(message)s'
        file_handler.setFormatter(Formatter(log_fmt))
        file_handler.level = DEBUG
        logger.addHandler(file_handler)
        logger.setLevel(DEBUG)
        logger.debug('Init')

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def _switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        self.logger.info('Switch ID : ' + str(datapath.id))
        if(datapath.id != 1):
            return
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        match = parser.OFPMatch()
        # アクションの指定・どのエントリにもマッチしなかった場合コントローラーにPacket-Inを送る
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions, 0)
        self.logger.debug('Controller Entry Initialzed')

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        self.logger.debug('Packet-In !!')
        msg = ev.msg
        datapath = msg.datapath
        port = msg.match['in_port']
        pkt = packet.Packet(data=msg.data)
        pkt_ethernet = pkt.get_protocol(ethernet.ethernet)
        if not pkt_ethernet:
            return
        SrcVlan = pkt.get_protocol(vlan.vlan)
        if SrcVlan is None:
            SrcVlan = 0
        else:
            SrcVlan = SrcVlan.vid
        self.logger.debug('SrcVLAN : ' + str(SrcVlan))
        pkt_ipv4 = pkt.get_protocol(ipv4.ipv4)
        if pkt_ipv4:
            if "192.168." in pkt_ipv4.dst[:8]:
                self.logger.warn("drop : " + pkt_ipv4.src + " ---> " + pkt_ipv4.dst)
                return
            IpSeg = pkt_ipv4.dst.split('.')
            if IpSeg[0] == "10" and 1 <= int(IpSeg[1]) and int(IpSeg[1]) <= 15:
                IpSeg = pkt_ipv4.src.split('.')
                if IpSeg[0] == "10" and 1 <= int(IpSeg[1]) and int(IpSeg[1]) <= 15:
                    return
                # 0.0.0.0から192.168.0.0/16へのアクセスを考える
                #self._handle_external_nat(datapath, port, pkt, pkt_ipv4, SrcVlan)
                return
            # 192.168.0.0/8から10.0.0.0/8へのアクセスを考える グローバル含む
            self._handle_internal_nat(datapath, port, pkt, pkt_ipv4, SrcVlan)

    def _handle_internal_nat(self, datapath, port, pkt, pkt_ipv4, SrcVlan):
        IpSeg = pkt_ipv4.src.split('.')
        NatIP = "10." + str(int(SrcVlan) // 100) + "." + IpSeg[2] + "." + IpSeg[3]
        self.logger.info('Internal Nat Vlan' + str(SrcVlan) + 'Rewrite IP : ' + NatIP)
        # **********************192.168.0.0/24から10.0.0.0/8*************************************
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(vlan_vid=(0x1000 | SrcVlan), in_port=1, eth_type=0x0800, ipv4_src=pkt_ipv4.src)
        out_port = 2
        actions = [parser.OFPActionPopVlan(), parser.OFPActionSetField(ipv4_src=NatIP), parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, 30000, match, actions, 60)
        self.logger.info('Match vlan : ' + str(SrcVlan) + ' src : ' + pkt_ipv4.src + ' --> Rewrite src : ' + NatIP)
        # **********************10.0.0.0/8から192.168.0.0./16************************************
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(in_port=2, eth_type=0x0800, ipv4_dst=NatIP)
        out_port = 1
        actions = [datapath.ofproto_parser.OFPActionPushVlan(ETH_TYPE_8021Q), parser.OFPActionSetField(vlan_vid=SrcVlan | ofproto_v1_3.OFPVID_PRESENT), parser.OFPActionSetField(ipv4_dst=pkt_ipv4.src), parser.OFPActionOutput(out_port)]
        # actions = [parser.OFPActionSetField(ipv4_dst=pkt_ipv4.src), parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, 30000, match, actions, 60)
        self.logger.info('Match dst : ' + NatIP + ' --> Rewrite dst : ' + pkt_ipv4.src)
        # ***************************************************************************************

    def _handle_external_nat(self, datapath, port, pkt, pkt_ipv4, SrcVlan):
        IpSeg = pkt_ipv4.dst.split('.')
        SrcVlan = int(IpSeg[1]) * 100
        NatIP = "192.168." + IpSeg[2] + "." + IpSeg[3]
        self.logger.info('External Nat Vlan' + str(SrcVlan) + 'Rewrite IP : ' + NatIP)
        # **********************10.0.0.0/8から192.168.0.0/16**************************************
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(in_port=2, eth_type=0x0800, ipv4_dst=pkt_ipv4.dst)
        out_port = 2
        actions = [datapath.ofproto_parser.OFPActionPushVlan(ETH_TYPE_8021Q), parser.OFPActionSetField(vlan_vid=SrcVlan | ofproto_v1_3.OFPVID_PRESENT),  parser.OFPActionSetField(ipv4_dst=NatIP), parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, 30000, match, actions, 60)
        self.logger.info('Match dst : ' + pkt_ipv4.dst + ' --> Rewrite dst : ' + NatIP)
        # **********************192.168.0.0/24から10.0.0.0/8**************************************
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(in_port=1, vlan_vid=(0x1000 | SrcVlan), eth_type=0x0800, ipv4_src=NatIP)
        out_port = 1
        actions = [parser.OFPActionPopVlan(), parser.OFPActionSetField(ipv4_src=pkt_ipv4.dst), parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, 30000, match, actions, 60)
        self.logger.info('Match vlan : ' + str(SrcVlan) + ' src : ' + NatIP + ' --> Rewrite src : ' + pkt_ipv4.dst)
        # ***************************************************************************************

    def add_flow(self, datapath, priority, match, actions, idle_timeout):
        # フローエントリ追加
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                match=match, instructions=inst, idle_timeout=idle_timeout)
        datapath.send_msg(mod)
