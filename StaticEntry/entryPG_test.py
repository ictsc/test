from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.topology import switches, event
from ryu.lib import ofctl_v1_3

# from ryu.lib.packet import packet
# from ryu.lib.packet import ethernet


class AddFlowEntry(app_manager.RyuApp):
    # OpenFlowのバージョン指定
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    # 初期化
    def __init__(self, *args, **kwargs):
        super(AddFlowEntry, self).__init__(*args, **kwargs)
        # スイッチID,Macaddressを管理するためのディクショナリ
        self.mac_to_port = {}

    # 初期エントリ追加
    # ryu.controller.handler.CONFIG_DISPATCHER : SwitchFeaturesメッセージの受信待ち
    @set_ev_cls(event.EventSwitchEnter)
    def _switch_enter_handler(self, ev):
        datapath = ev.switch.dp
        group = {
            'group_id': 1,
            'type': 'SELECT',
            'buckets': [
                {
                    'weight': 90,
                    'actions': [
                        {
                            'type': 'OUTPUT',
                            'port': ofproto_v1_3.OFPP_FLOOD,
                        },
                    ],
                },
                {
                    'weight': 10,
                    'actions': [],
                },
            ],
        }
        ofctl_v1_3.mod_group_entry(
            datapath,
            group,
            ofproto_v1_3.OFPGC_ADD,
        )
        flow = {
            'actions': [
                {
                    'type': 'GROUP',
                    'group_id': 1,
                },
            ]
        }
        ofctl_v1_3.mod_flow_entry(
            datapath,
            flow,
            ofproto_v1_3.OFPFC_ADD,
        )

    # フローエントリ追加メッセージ送信処理
    def add_flow(self, datapath, priority, match, actions, idle_timeout):
        """
        datapath : Datapath
        priority : 最大65535 大きいほど優先される
        match : 来たパケットのマッチ条件
        actions : マッチしたパケットをどうするか
        idle_timeout : 登録したフローエントリがマッチしなくなってから消える時間,0だと消えない
        """
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # インストラクションを設定する
        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                match=match, instructions=inst, idle_timeout=idle_timeout)
        # フローエントリを追加する
        datapath.send_msg(mod)


"""
ofproto内のパラメーター
ryu/ofproto/ofproto_v1_3.py
に書いてある全て
"""

"""
# Matchフィールド名	説明
# in_port           受信ポートのポート番号
# in_phy_port       受信ポートの物理ポート番号
# metadata          テーブル間で情報を受け渡すために用いられるメタデータ
# eth_dst           Ethernetの宛先MACアドレス
# eth_src           Ethernetの送信元MACアドレス
# eth_type          Ethernetのフレームタイプ
# vlan_vid			VLAN ID
# vlan_pcp			VLAN PCP
# ip_dscp	        IP DSCP
# ip_ecn	        IP ECN
# ip_proto			IPのプロトコル種別
# ipv4_src			IPv4の送信元IPアドレス
# ipv4_dst			IPv4の宛先IPアドレス
# tcp_src	        TCPの送信元ポート番号
# tcp_dst	        TCPの宛先ポート番号
# udp_src	        UDPの送信元ポート番号
# udp_dst	        UDPの宛先ポート番号
# sctp_src			SCTPの送信元ポート番号
# sctp_dst		    SCTPの宛先ポート番号
# icmpv4_type	    ICMPのType
# icmpv4_code	    ICMPのCode
# arp_op	        ARPのオペコード
# arp_spa	        ARPの送信元IPアドレス
# arp_tpa	        ARPのターゲットIPアドレス
# arp_sha	        ARPの送信元MACアドレス
# arp_tha	        ARPのターゲットMACアドレス
# ipv6_src	        IPv6の送信元IPアドレス
# ipv6_dst	        IPv6の宛先IPアドレス
# ipv6_flabel	    IPv6のフローラベル
# icmpv6_type	    ICMPv6のType
# icmpv6_code	    ICMPv6のCode
# ipv6_nd_target	IPv6ネイバーディスカバリのターゲットアドレス
# ipv6_nd_sll	    IPv6ネイバーディスカバリの送信元リンクレイヤーアドレス
# ipv6_nd_tll	    IPv6ネイバーディスカバリのターゲットリンクレイヤーアドレス
# mpls_label	    MPLSのラベル
# mpls_tc	        MPLSのトラフィッククラス(TC)
# mpls_bos	        MPLSのBoSビット
# pbb_isid	        802.1ah PBBのI-SID
# tunnel_id	        論理ポートに関するメタデータ
# ipv6_exthdr	    IPv6の拡張ヘッダの擬似フィールド
"""


"""
OFPActionOutputで使えるモノ

OFPP_IN_PORT        受信ポートに転送されます
OFPP_TABLE          先頭のフローテーブルに摘要されます
OFPP_NORMAL         スイッチのL2/L3機能で転送されます
OFPP_FLOOD          受信ポートやブロックされているポートを除く当該VLAN内のすべての物理ポートにフラッディングされます
OFPP_ALL            受信ポートを除くすべての物理ポートに転送されます
OFPP_CONTROLLER     コントローラにPacket-Inメッセージとして送られます
OFPP_LOCAL          スイッチのローカルポートを示します
OFPP_ANY            Flow Mod(delete)メッセージやFlow Stats Requestsメッセージでポートを選択する際にワイルドカードとして使用するもので、パケット転送では使用されません
"""
