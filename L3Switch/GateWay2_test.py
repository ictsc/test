from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import arp
from ryu.lib.packet import ipv4
from ryu.lib.packet import icmp


class IcmpResponder(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(IcmpResponder, self).__init__(*args, **kwargs)
        self.ip_addr = '172.16.0.1'
        # {"GroupName": {PortNum: [["VM1-IP","VM1-MAC"],["VM2-IP","VM2-MAC"], .. ] PortNum2 }, GN2 }
        self.port_group = {"G1": {1: [["172.16.0.2", "0a:e4:1c:d1:3e:4"]]}, "G2": {2: [["112.16.1.1", "0a:e4:1c:d1:3e:5"]]}}
        self.group_gate = {"G1": ["172.16.0.1", "0a:e4:1c:d1:3e:44", "172.16.0"], "G2": ["172.16.1.1", "0a:e4:1c:d1:3e:45", "172.16.1"]}
        self.permit_group = {"G1": ["G1", "G2"], "G2": ["G1", "G2"]}

    # FindPort = lambda  PortGroup, DstGroup, DstIp : [i for i in PortGroup[DstGroup].keys() if [k for k in PortGroup[DstGroup][i] if k[0] == DstIp]]

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def _switch_features_handler(self, ev):
        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        # バッファしないでコントローラーに送るエントリーを追加
        actions = [parser.OFPActionOutput(port=ofproto.OFPP_CONTROLLER,
                                          max_len=ofproto.OFPCML_NO_BUFFER)]
        inst = [parser.OFPInstructionActions(type_=ofproto.OFPIT_APPLY_ACTIONS,
                                             actions=actions)]
        mod = parser.OFPFlowMod(datapath=datapath,
                                priority=0,
                                match=parser.OFPMatch(),
                                instructions=inst)
        datapath.send_msg(mod)

    # スイッチが聞きに来た時
    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        msg = ev.msg
        datapath = msg.datapath
        # そのパケットが来たポート
        port = msg.match['in_port']
        pkt = packet.Packet(data=msg.data)
        self.logger.info("packet-in %s" % (pkt,))
        # Macアドレス情報
        pkt_ethernet = pkt.get_protocol(ethernet.ethernet)
        if not pkt_ethernet:
            return
        # Arp情報
        pkt_arp = pkt.get_protocol(arp.arp)
        if pkt_arp:
            # ARP情報があった場合、ARPリクエストだった場合はARPリクエストを返す
            self._handle_arp(datapath, port, pkt_ethernet, pkt_arp)
            return
        # IP情報を取得する
        pkt_ipv4 = pkt.get_protocol(ipv4.ipv4)
        pkt_icmp = pkt.get_protocol(icmp.icmp)
        if pkt_icmp:
            # ICMP情報があった場合、ICMPリクエストだった場合はICMPを返す
            self._handle_icmp(datapath, port, pkt_ethernet, pkt_ipv4, pkt_icmp)
            return

    def _handle_arp(self, datapath, port, pkt_ethernet, pkt_arp):
        # パケットがARPリクエストでなかった場合はすぐに返す
        if pkt_arp.opcode != arp.ARP_REQUEST:
            return
        # 送信元IPがどこのゲートウェイに属しているか、調べる 前提として送信元でドロップする
        SrcGroup = [i for i in self.group_gate if ''.join(self.group_gate[i][2].split(".")) in ''.join(pkt_arp.src_ip.split("."))]
        # 探しているゲートウェイが見つからなかったら、何もしない
        if len(SrcGroup) == 0:
            return
        SrcGroup = SrcGroup[0]
        # 所属Groupの中に探しているゲートウェイIPアドレスをもっているか確認する
        if self.group_gate[SrcGroup][0] not in pkt_arp.dst_ip:
            # 別ポートにつないでるところのMACアドレスは静的にフローエントリを先に登録して繋げておくべき
            return
        # 来たMACやIPが未登録なら、そいつを登録する
        if [i for i in self.port_group[SrcGroup][int(port)] if pkt_arp.src_mac in i[1]] is None:
            self.port_group[SrcGroup][int(port)].append([pkt_arp.src_ip, pkt_arp.src_mac])
        # ARPリプライを生成する
        pkt = packet.Packet()
        pkt.add_protocol(ethernet.ethernet(ethertype=pkt_ethernet.ethertype,
                                           dst=pkt_ethernet.src,
                                           src=self.group_gate[SrcGroup][1]))  # ゲートウェイのmac
        pkt.add_protocol(arp.arp(opcode=arp.ARP_REPLY,
                                 src_mac=self.group_gate[SrcGroup][1],  # ゲートウェイのmac
                                 src_ip=self.group_gate[SrcGroup][0],  # ゲートウェイのIP
                                 dst_mac=pkt_arp.src_mac,
                                 dst_ip=pkt_arp.src_ip))
        # パケットを送信する
        self._send_packet(datapath, port, pkt)

    def _handle_icmp(self, datapath, port, pkt_ethernet, pkt_ipv4, pkt_icmp):
        # パケットがICMP ECHOリクエストでなかった場合はすぐに返す
        if pkt_icmp.type != icmp.ICMP_ECHO_REQUEST:
            return
        # まずはそいつのIPがどこのゲートウェイに属しているか、調べる  前提としてソースでドロップする
        SrcGroup = [i for i in self.group_gate if ''.join(self.group_gate[i][2].split(".")) in ''.join(pkt_ipv4.src_ip.split("."))]
        # 探しているゲートウェイが見つからなかったら、何もしない
        if len(SrcGroup) == 0:
            return
        SrcGroup = SrcGroup[0]
        #  宛先IPがどこのゲートウェイに属しているか、調べる
        DstGroup = [i for i in self.group_gate if ''.join(self.group_gate[i][2].split(".")) in ''.join(pkt_ipv4.dst_ip.split("."))]
        # 通信が許可されたネットワークでなければ、何もせず終了
        if DstGroup not in self.permit_group[SrcGroup]:
            return
        # 宛先がゲートウェイか確認する
        if self.group_gate[SrcGroup][0] not in pkt_ipv4.dst_ip:
            # 本当ならここでルーティング処理を行い、フローエントリを追加してあげる
            # 宛先ネットワークでdst_macが登録されてない場合はFlood処理してあげる必要がある
            DstInfo = [j for j in [i for i in self.port_group[DstGroup]] if j[1] == pkt_ipv4.dst_ip]
            # 宛先があるかないか
            if len(DstInfo) == 0:
                # 所属してるPort全てにFlood処理をしてあげる
                # arp リクエスト
                for i in self.port_group[DstGroup].keys():
                    self._arp_request(datapath, i, pkt_ethernet, pkt_ipv4, SrcGroup)
                return
            # 宛先が見つかったのでルーティング処理をしてあげる
            DstPort = [i for i in self.port_group[DstGroup].keys() if [k for k in self.port_group[DstGroup][i] if k[0] == pkt_ipv4.dst_ip]][0]
            print(DstPort)
            # 送信元をゲートウェイのmacアドレスに変えて送る(IPはそのまま
            mac_dst = pkt_ethernet.dst
            mac_src = self.group_gate[SrcGroup][1]
            ip_dst = pkt_ipv4.dst
            ip_src = pkt_ipv4.src
            # フローエントリを登録すべき！！！！！！！！！！！！！！
            self._icmp_reply(datapath, port, pkt_ethernet, pkt_ipv4, pkt_icmp, mac_dst, mac_src, ip_dst)
            return
        # 宛先がゲートェイならばICMPを作成して返す
        mac_dst = pkt_ethernet.src
        mac_src = self.group_gate[SrcGroup][1]
        ip_dst = pkt_ipv4.src
        ip_src = self.group_gate[SrcGroup][0]
        self._icmp_reply(datapath, port, pkt_ethernet, pkt_ipv4, pkt_icmp, mac_dst, mac_src, ip_dst, ip_src)

    def _send_packet(self, datapath, port, pkt):
        # 作られたパケットをOut-Packetメッセージ送り送信する
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        pkt.serialize()
        self.logger.info("packet-out %s" % (pkt,))
        data = pkt.data
        actions = [parser.OFPActionOutput(port=port)]
        out = parser.OFPPacketOut(datapath=datapath,
                                  buffer_id=ofproto.OFP_NO_BUFFER,
                                  in_port=ofproto.OFPP_CONTROLLER,
                                  actions=actions,
                                  data=data)
        datapath.send_msg(out)

    def _arp_request(self, datapath, port, pkt_ethernet, pkt_ipv4, SrcGroup):
        pkt = packet.Packet()
        pkt.add_protocol(ethernet.ethernet(ethertype=pkt_ethernet.ethertype,
                                           dst=pkt_ipv4.dst_ip,
                                           src=self.group_gate[SrcGroup][1]))  # ゲートウェイのmac
        pkt.add_protocol(arp.arp(opcode=arp.ARP_REQUEST,
                                 src_mac=self.group_gate[SrcGroup][1],  # ゲートウェイのmac
                                 src_ip=self.group_gate[SrcGroup][0],  # ゲートウェイのIP
                                 dst_mac='ff:ff:ff:ff:ff:ff',
                                 dst_ip=pkt_ipv4.dst_ip))
        # パケットを送信する
        self._send_packet(datapath, port, pkt)

    def _icmp_reply(self, datapath, port, pkt_ethernet, pkt_ipv4, pkt_icmp, mac_dst, mac_src, ip_dst, ip_src):
        pkt = packet.Packet()
        pkt.add_protocol(ethernet.ethernet(ethertype=pkt_ethernet.ethertype,
                                           dst=mac_dst,
                                           src=mac_src))  # ゲートウェイのmac
        pkt.add_protocol(ipv4.ipv4(dst=ip_dst,
                                   src=ip_src,  # ゲートウェイのIP
                                   proto=pkt_ipv4.proto))
        pkt.add_protocol(icmp.icmp(type_=icmp.ICMP_ECHO_REPLY,
                                   code=icmp.ICMP_ECHO_REPLY_CODE,
                                   csum=0,
                                   data=pkt_icmp.data))
        self._send_packet(datapath, port, pkt)


"""

class AddFlowEntry(app_manager.RyuApp):
    # OpenFlowのバージョン指定
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    # 初期化
    def __init__(self, *args, **kwargs):
        super(AddFlowEntry, self).__init__(*args, **kwargs)
        # スイッチID,Macaddressを管理するためのディクショナリ
        self.mac_to_port = {}

    # 初期エントリ追加
    # ryu.controller.handler.CONFIG_DISPATCHER : SwitchFeaturesメッセージの受信待ち
    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        # OpenFlowスイッチとの実際の通信処理や受信メッセージに対応したイベントの発行など
        datapath = ev.msg.datapath
        print(datapath.id)
        # 使用しているOpenFlowバージョンに対応したofprotoモジュールを示します
        # ofproto = datapath.ofproto
        # ofprotoと同様に、ofproto_parserモジュール
        parser = datapath.ofproto_parser
        # 例) match = parser.OFPMatch(in_port=in_port,  eth_dst=dst)
        match = parser.OFPMatch(in_port=2)
        # 例) actions = [parser.OFPActionOutput(out_port)]
        out_port = 1
        actions = [parser.OFPActionOutput(out_port)]
        # フローエントリを追加
        self.add_flow(datapath, 30000, match, actions, 0)

        datapath = ev.msg.datapath
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(in_port=1)
        out_port = 2
        actions = [parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, 30001, match, actions, 0)

    # フローエントリ追加メッセージ送信処理
    def add_flow(self, datapath, priority, match, actions, idle_timeout):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # インストラクションを設定する
        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                match=match, instructions=inst, idle_timeout=idle_timeout)
        # フローエントリを追加する
        datapath.send_msg(mod)

"""

"""
ofproto内のパラメーター
ryu/ofproto/ofproto_v1_3.py
に書いてある全て
"""

"""
# Matchフィールド名	説明
# in_port           受信ポートのポート番号
# in_phy_port       受信ポートの物理ポート番号
# metadata          テーブル間で情報を受け渡すために用いられるメタデータ
# eth_dst           Ethernetの宛先MACアドレス
# eth_src           Ethernetの送信元MACアドレス
# eth_type          Ethernetのフレームタイプ
# vlan_vid			VLAN ID
# vlan_pcp			VLAN PCP
# ip_dscp	        IP DSCP
# ip_ecn	        IP ECN
# ip_proto			IPのプロトコル種別
# ipv4_src			IPv4の送信元IPアドレス
# ipv4_dst			IPv4の宛先IPアドレス
# tcp_src	        TCPの送信元ポート番号
# tcp_dst	        TCPの宛先ポート番号
# udp_src	        UDPの送信元ポート番号
# udp_dst	        UDPの宛先ポート番号
# sctp_src			SCTPの送信元ポート番号
# sctp_dst		    SCTPの宛先ポート番号
# icmpv4_type	    ICMPのType
# icmpv4_code	    ICMPのCode
# arp_op	        ARPのオペコード
# arp_spa	        ARPの送信元IPアドレス
# arp_tpa	        ARPのターゲットIPアドレス
# arp_sha	        ARPの送信元MACアドレス
# arp_tha	        ARPのターゲットMACアドレス
# ipv6_src	        IPv6の送信元IPアドレス
# ipv6_dst	        IPv6の宛先IPアドレス
# ipv6_flabel	    IPv6のフローラベル
# icmpv6_type	    ICMPv6のType
# icmpv6_code	    ICMPv6のCode
# ipv6_nd_target	IPv6ネイバーディスカバリのターゲットアドレス
# ipv6_nd_sll	    IPv6ネイバーディスカバリの送信元リンクレイヤーアドレス
# ipv6_nd_tll	    IPv6ネイバーディスカバリのターゲットリンクレイヤーアドレス
# mpls_label	    MPLSのラベル
# mpls_tc	        MPLSのトラフィッククラス(TC)
# mpls_bos	        MPLSのBoSビット
# pbb_isid	        802.1ah PBBのI-SID
# tunnel_id	        論理ポートに関するメタデータ
# ipv6_exthdr	    IPv6の拡張ヘッダの擬似フィールド
"""


"""
OFPActionOutputで使えるモノ

OFPP_IN_PORT        受信ポートに転送されます
OFPP_TABLE          先頭のフローテーブルに摘要されます
OFPP_NORMAL         スイッチのL2/L3機能で転送されます
OFPP_FLOOD          受信ポートやブロックされているポートを除く当該VLAN内のすべての物理ポートにフラッディングされます
OFPP_ALL            受信ポートを除くすべての物理ポートに転送されます
OFPP_CONTROLLER     コントローラにPacket-Inメッセージとして送られます
OFPP_LOCAL          スイッチのローカルポートを示します
OFPP_ANY            Flow Mod(delete)メッセージやFlow Stats Requestsメッセージでポートを選択する際にワイルドカードとして使用するもので、パケット転送では使用されません
"""
