# from ryu.controller import ofp_event
from ryu.ofproto import ofproto_v1_3
# from ryu.lib.packet import packet
# from ryu.lib.packet import ethernet
# from ryu.lib.packet import arp
# from ryu.lib.packet import ipv4
# from ryu.lib.packet import icmp
# from ryu.lib.packet import vlan
from ryu.lib import ofctl_v1_3


class CreateEntry():

    def dhcp_flow(self, datapath):
        group = {
            'group_id': 1,
            'type': 'ALL',
            'buckets': [
                {
                    'weight': 100,
                    'actions': [
                        {
                            'type': 'OUTPUT',
                            'port': 1,
                        },
                    ],
                },
                {
                    'weight': 100,
                    'actions': [
                        {
                            'type': 'OUTPUT',
                            'port': 2,
                        },
                    ],
                },
            ],
        }
        ofctl_v1_3.mod_group_entry(
            datapath,
            group,
            ofproto_v1_3.OFPGC_ADD,
        )
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(in_port=1, eth_type=0x0800, ip_proto=17, udp_src=67)
        actions = [parser.OFPActionGroup(group_id=1)]
        self.add_flow(datapath, 30000, match, actions, 0)
        match = parser.OFPMatch(in_port=2, eth_type=0x0800, ip_proto=17, udp_src=68)
        actions = [parser.OFPActionGroup(group_id=1)]
        self.add_flow(datapath, 30000, match, actions, 0)
        match = parser.OFPMatch(in_port=2, eth_type=0x0800, ip_proto=17, udp_src=67)
        actions = [parser.OFPActionGroup(group_id=1)]
        self.add_flow(datapath, 30000, match, actions, 0)
        match = parser.OFPMatch(in_port=1, eth_type=0x0800, ip_proto=17, udp_src=68)
        actions = [parser.OFPActionGroup(group_id=1)]
        self.add_flow(datapath, 30000, match, actions, 0)
        print('DHCP Entried')
        # match = parser.OFPMatch(in_port=1, eth_type=0x0800, ip_proto=17, udp_src=68)
        # out_port = 2
        # actions = [parser.OFPActionOutput(out_port)]
        # self.add_flow(datapath, 30000, match, actions, 0)

    def ip_flow(self, datapath):
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(eth_type=0x0800, ipv4_dst='172.16.0.3')
        out_port = 2
        actions = [parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, 30000, match, actions, 0)
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(eth_type=0x0800, ipv4_dst='172.16.0.2')
        out_port = 1
        actions = [parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, 30001, match, actions, 0)
        print('IP Entried')

    # フローエントリ追加メッセージ送信処理
    def add_flow(self, datapath, priority, match, actions, idle_timeout):
        """
        datapath : Datapath
        priority : 最大65535 大きいほど優先される
        match : 来たパケットのマッチ条件
        actions : マッチしたパケットをどうするか
        idle_timeout : 登録したフローエントリがマッチしなくなってから消える時間,0だと消えない
        """
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # インストラクションを設定する
        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                match=match, instructions=inst, idle_timeout=idle_timeout)
        # フローエントリを追加する
        datapath.send_msg(mod)

    def _handle_internal_nat(self, datapath, port, pkt, pkt_ethernet, pkt_ipv4, SrcGroup, SrcVlan):
        # 192.168.0.0/24から10.0.0.0/8
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(eth_type=0x0800, ipv4_dst=('10.0.0.0', '255.0.0.0'))
        out_port = 2
        # macをゲートウェイに書き換え、IPを適切に変換、VLANをSET、アウトポートを指示
        actions = [parser.OFPActionSetField(src_dst='02:42:0a:01:00:03', ipv4_src=pkt_ipv4.dst), parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, 30000, match, actions, 0)

        # 10.0.0.0/8から192.168.0.0./16
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(eth_type=0x0800, ipv4_dst='172.16.0.3')
        out_port = 2
        actions = [parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, 30000, match, actions, 0)

    def _handle_external_nat(self, datapath, port, pkt, pkt_ethernet, pkt_ipv4, SrcGroup, SrcVlan):
        # 10.0.0.0/8から192.168.0.0./16
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(eth_type=0x0800, ipv4_dst='172.16.0.3')
        out_port = 2
        actions = [parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, 30000, match, actions, 0)

        # 192.168.0.0/24から10.0.0.0/8
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(eth_type=0x0800, ipv4_dst='172.16.0.3')
        out_port = 2
        actions = [parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, 30000, match, actions, 0)

    def _handle_global_nat(self, datapath, port, pkt, pkt_ethernet, pkt_ipv4, SrcGroup, SrcVlan):
        # 内部からグローバルルータに対して送る
        # 192.168.0.0/24から0.0.0.0/0
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(eth_type=0x0800, ipv4_dst='172.16.0.3')
        out_port = 2
        actions = [parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, 30000, match, actions, 0)

        # 10.0.0.1/8から192.168.0.0./16
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(eth_type=0x0800, ipv4_dst='172.16.0.3')
        out_port = 2
        actions = [parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, 30000, match, actions, 0)
