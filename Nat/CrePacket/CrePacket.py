from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import arp
from ryu.lib.packet import ipv4
from ryu.lib.packet import icmp
from ryu.lib.packet import vlan
from ryu.lib import ofctl_v1_3
# DHCPパケット通過フローエントリを追加


class CreatePacket():

    def _handle_arp(self, datapath, port, pkt_ethernet, pkt_arp, SrcGroup):
        # 送信元Mac,IPアドレスがすでに登録されているか探す
        SrcIP = [i for i in self.group_mac[SrcGroup] if pkt_arp.src_ip == i[0]]
        # なかった場合は登録する
        if len(SrcIP) == 0:
            self.group_mac[SrcGroup].append([pkt_arp.src_ip, pkt_arp.src_mac, port])
        # 宛先IPを探す
        DstIPMAC = [i for i in self.group_mac[SrcGroup] if i[0] == pkt_arp.dst_ip]
        print('DstIPMAC :', DstIPMAC)
        # パケットがARPリクエストの場合の処理
        if pkt_arp.opcode == arp.ARP_REQUEST:
            if len(DstIPMAC) == 0:
                # IPがなかった時はFlood処理をしてあげる
                for port in self.group_port[SrcGroup]:
                    self._arp_request(datapath, port, pkt_ethernet, pkt_arp.src_mac, pkt_arp.dst_ip, pkt_arp.src_ip, SrcGroup)
                return
            else:
                DstIPMAC = DstIPMAC[0]
                # IPを知っている場合は代わりにarpリプライを返す
                # ゲートウェイに対するARPリクエストもARPリプライを返してあげる
                self._arp_reply(datapath, port, pkt_ethernet, pkt_arp.src_mac, DstIPMAC[1], pkt_arp.src_ip, DstIPMAC[0], SrcGroup)
                return
        # パケットがARPリプライの場合の処理
        # L3のreply先がゲートウェイである場合は返す必要がない
        if pkt_arp.opcode == arp.ARP_REPLY and pkt_arp.dst_mac != self.gateway_mac:
            # ARPリプライで学習されているはずなのに宛先が見つからないおかしなパケットの処理
            if len(DstIPMAC) == 0:
                return
            DstIPMAC = DstIPMAC[0]
            # ARPリプライの場合すでに宛先は学習されている
            self._arp_reply(datapath, DstIPMAC[2], pkt_ethernet, pkt_arp.dst_mac, pkt_arp.src_mac, pkt_arp.dst_ip, pkt_arp.src_ip, SrcGroup)
            return
        return

    def _arp_reply(self, datapath, port, pkt_ethernet, dst_mac, src_mac, dst_ip, src_ip, SrcGroup):
        # ARPリプライを生成する
        print('ARP Reply : ', src_ip, ' > ', dst_ip)
        pkt = packet.Packet()
        pkt.add_protocol(ethernet.ethernet(ethertype=pkt_ethernet.ethertype,
                                           dst=pkt_ethernet.src,
                                           src=src_mac))
        pkt.add_protocol(arp.arp(opcode=arp.ARP_REPLY,
                                 src_mac=src_mac,
                                 src_ip=src_ip,
                                 dst_mac=dst_mac,
                                 dst_ip=dst_ip))
        # パケットを送信する
        # 送るportがSrcGroupによってどのVLANがつくかどうか調べてあげる必要がある
        self._send_packet(datapath, port, pkt)

    def _arp_request(self, datapath, port, pkt_ethernet, src_mac, dst_ip, src_ip, DstGroup):
        # ARPリクエストを生成する
        print('ARP Request : ', src_ip, ' > ', dst_ip)
        pkt = packet.Packet()
        pkt.add_protocol(ethernet.ethernet(ethertype=pkt_ethernet.ethertype,
                                           dst=pkt_ethernet.dst,
                                           src=pkt_ethernet.src))
        pkt.add_protocol(arp.arp(opcode=arp.ARP_REQUEST,
                                 src_mac=src_mac,
                                 src_ip=src_ip,
                                 dst_mac='ff:ff:ff:ff:ff:ff',
                                 dst_ip=dst_ip))
        # パケットを送信する
        # 送るportがDstGroupによってどのVLANがつくかどうか調べてあげる必要がある
        self._send_packet(datapath, port, pkt)

    def _handle_icmp(self, datapath, port, pkt_ethernet, pkt_ipv4, pkt_icmp, SrcGroup):
        # パケットがICMP ECHOリクエストでなかった場合はすぐに返す
        # 自分のゲートウェイIPアドレスをもっているグループでなかったら終了
        print('ICMP : ', pkt_ipv4.src, ' > ', pkt_ipv4.dst)
        if pkt_icmp.type != icmp.ICMP_ECHO_REQUEST or pkt_ipv4.dst != self.group_mac[SrcGroup][0][0]:
            return
        # ICMPを作成して返す
        pkt = packet.Packet()
        pkt.add_protocol(ethernet.ethernet(ethertype=pkt_ethernet.ethertype,
                                           dst=pkt_ethernet.src,
                                           src=self.gateway_mac))  # ゲートウェイのmac
        pkt.add_protocol(ipv4.ipv4(dst=pkt_ipv4.src,
                                   src=self.group_mac[SrcGroup][0][0],  # ゲートウェイのIP
                                   proto=pkt_ipv4.proto))
        pkt.add_protocol(icmp.icmp(type_=icmp.ICMP_ECHO_REPLY,
                                   code=icmp.ICMP_ECHO_REPLY_CODE,
                                   csum=0,
                                   data=pkt_icmp.data))
        self._send_packet(datapath, port, pkt)

    def _send_packet(self, datapath, port, pkt):
        # 作られたパケットをOut-Packetメッセージ送り送信する
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        pkt.serialize()
        # self.logger.info("packet-out %s" % (pkt,))
        data = pkt.data
        actions = [parser.OFPActionOutput(port=port)]
        out = parser.OFPPacketOut(datapath=datapath,
                                  buffer_id=ofproto.OFP_NO_BUFFER,
                                  in_port=ofproto.OFPP_CONTROLLER,
                                  actions=actions,
                                  data=data)
        datapath.send_msg(out)
