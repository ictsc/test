from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ipv4
from ryu.lib.packet import vlan
# from ryu.lib import ofctl_v1_3


class GateWay(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(GateWay, self).__init__(*args, **kwargs)

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def _switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        print(datapath.id, ': 1')
        if(datapath.id != 1):
            return

        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        # バッファしないでコントローラーに送るエントリーを追加
        actions = [parser.OFPActionOutput(port=ofproto.OFPP_CONTROLLER,
                                          max_len=ofproto.OFPCML_NO_BUFFER)]
        inst = [parser.OFPInstructionActions(type_=ofproto.OFPIT_APPLY_ACTIONS,
                                             actions=actions)]
        mod = parser.OFPFlowMod(datapath=datapath,
                                priority=0,
                                match=parser.OFPMatch(),
                                instructions=inst)
        datapath.send_msg(mod)
        print('Initialze End')

    # スイッチが聞きに来た時
    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        msg = ev.msg
        datapath = msg.datapath
        # そのパケットが来たポート
        port = msg.match['in_port']
        pkt = packet.Packet(data=msg.data)
        # self.logger.info("packet-in %s" % (pkt,))
        # Macアドレス情報
        pkt_ethernet = pkt.get_protocol(ethernet.ethernet)
        if not pkt_ethernet:
            return
        # トランクポートの時にどこのチームVLANに所属しているかを確認する
        SrcVlan = pkt.get_protocol(vlan.vlan)
        print(SrcVlan)
        if SrcVlan == None:
            SrcVlan = 0
        # IP情報を取得する
        pkt_ipv4 = pkt.get_protocol(ipv4.ipv4)
        if pkt_ipv4:
            print(pkt_ipv4.src, "--->", pkt_ipv4.dst)
            if "192.168." in pkt_ipv4.dst[:8]:
                print("drop : ", pkt_ipv4.src, "--->", pkt_ipv4.dst)
                return
            IpSeg = pkt_ipv4.dst.split('.')
            if IpSeg[0] == "10" and 1 <= int(IpSeg[1]) and int(IpSeg[1]) <= 15:
                # 0.0.0.0から192.168.0.0/16へのアクセスを考える
                self._handle_external_nat(datapath, port, pkt, pkt_ipv4, SrcVlan)
            # if IpSeg[0] == "10" and 1 > int(IpSeg[1]) and int(IpSeg[1]) > 15:
            # 192.168.0.0/8から10.0.0.0/8へのアクセスを考える グローバル含む
            self._handle_internal_nat(datapath, port, pkt, pkt_ipv4, SrcVlan)

    def _handle_internal_nat(self, datapath, port, pkt, pkt_ipv4, SrcVlan):
        IpSeg = pkt_ipv4.dst.split('.')
        NatIP = "10." + str(int(SrcVlan) // 100) + "." + IpSeg[2] + "." + IpSeg[3]
        print(NatIP)

        # 192.168.0.0/24から10.0.0.0/8
        parser = datapath.ofproto_parser
        # VLANでマッチさせてあげる させてない
        match = parser.OFPMatch(in_port=1, eth_type=0x0800, ipv4_src=pkt_ipv4.src)
        out_port = 2
        # macをゲートウェイに書き換え、IPを適切に変換、VLANをSET、アウトポートを指示
        actions = [parser.OFPActionSetField(ipv4_src='10.0.0.2'), parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, 30000, match, actions, 0)

        # 10.0.0.0/8から192.168.0.0./16
        parser = datapath.ofproto_parser
        # この時の変換したsrcが宛先 src
        match = parser.OFPMatch(in_port=2, eth_type=0x0800, ipv4_dst='10.0.0.2')
        out_port = 1
        actions = [parser.OFPActionSetField(ipv4_dst='192.168.0.2'), parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, 30000, match, actions, 0)

    def _handle_external_nat(self, datapath, port, pkt, pkt_ipv4, SrcVlan):
        IpSeg = pkt_ipv4.dst.split('.')
        NatIP = "192.168." + IpSeg[2] + "." + IpSeg[3]
        print(NatIP)

        # 10.0.0.0/8から192.168.0.0/16
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(eth_type=0x0800, ipv4_dst=pkt_ipv4.dst)
        out_port = 2
        # Vlanをつけてあげる  今はあげてない
        actions = [parser.OFPActionSetField(ipv4_dst=NatIP), parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, 30000, match, actions, 0)

        # 192.168.0.0/24から10.0.0.0/8
        parser = datapath.ofproto_parser
        # VLANでマッチさせてあげる させてない
        match = parser.OFPMatch(eth_type=0x0800, ipv4_src=NatIP)
        out_port = 1
        actions = [parser.OFPActionSetField(ipv4_src=pkt_ipv4.dst), parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, 30000, match, actions, 0)

    def _send_packet(self, datapath, port, pkt):
        # 作られたパケットをOut-Packetメッセージ送り送信する
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        pkt.serialize()
        # self.logger.info("packet-out %s" % (pkt,))
        data = pkt.data
        actions = [parser.OFPActionOutput(port=port)]
        out = parser.OFPPacketOut(datapath=datapath,
                                  buffer_id=ofproto.OFP_NO_BUFFER,
                                  in_port=ofproto.OFPP_CONTROLLER,
                                  actions=actions,
                                  data=data)
        datapath.send_msg(out)

    # フローエントリ追加メッセージ送信処理
    def add_flow(self, datapath, priority, match, actions, idle_timeout):
        """
        datapath : Datapath
        priority : 最大65535 大きいほど優先される
        match : 来たパケットのマッチ条件
        actions : マッチしたパケットをどうするか
        idle_timeout : 登録したフローエントリがマッチしなくなってから消える時間,0だと消えない
        """
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # インストラクションを設定する
        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                match=match, instructions=inst, idle_timeout=idle_timeout)
        # フローエントリを追加する
        datapath.send_msg(mod)
