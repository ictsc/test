from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import arp
from ryu.lib.packet import ipv4
from ryu.lib.packet import icmp
from ryu.lib.packet import vlan
# from ryu.lib import ofctl_v1_3
from CrePacket.CrePacket import CreatePacket
from FlowEntry.CreateEntry import CreateEntry


class GateWay(app_manager.RyuApp, CreatePacket, CreateEntry):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(GateWay, self).__init__(*args, **kwargs)
        # ゲートウェイのMacアドレス
        self.gateway_mac = '0a:e4:1c:d1:3e:44'
        # ポートがポートVLANなのかトランクVLANなのか  True = トランク
        self.port_vlan = {'1': False, '2': False, '3': False}
        # チームごとのポートグループ
        self.group_port = {"T1": [1], "T2": [2]}
        # そのチームのトランク用のVLAN
        self.group_vlan = {"T1": 100, "T2": 10}
        # チームごとのIP,Macとそれが所属しているポート
        self.group_mac = {"T1": [['192.168.0.1', '0a:e4:1c:d1:3e:44', 0]], "T2": [['172.16.1.1', '0a:e4:1c:d1:3e:44', 0]]}

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def _switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        print(datapath.id, ': 1')
        if(datapath.id != 1):
            return
        self.dhcp_flow(datapath)
        self.ip_flow(datapath)

        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        # バッファしないでコントローラーに送るエントリーを追加
        actions = [parser.OFPActionOutput(port=ofproto.OFPP_CONTROLLER,
                                          max_len=ofproto.OFPCML_NO_BUFFER)]
        inst = [parser.OFPInstructionActions(type_=ofproto.OFPIT_APPLY_ACTIONS,
                                             actions=actions)]
        mod = parser.OFPFlowMod(datapath=datapath,
                                priority=0,
                                match=parser.OFPMatch(),
                                instructions=inst)
        datapath.send_msg(mod)
        print('Initialze End')

    # スイッチが聞きに来た時
    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        msg = ev.msg
        datapath = msg.datapath
        # そのパケットが来たポート
        port = msg.match['in_port']
        pkt = packet.Packet(data=msg.data)
        # self.logger.info("packet-in %s" % (pkt,))
        # Macアドレス情報
        pkt_ethernet = pkt.get_protocol(ethernet.ethernet)
        if not pkt_ethernet:
            return
        # 送信元ポートで判断する トランクの時は複数のグループが入る
        SrcGroup = [i for i in self.group_port if port in self.group_port[i]]
        # 登録されていないおかしなポートから来たら破棄
        if len(SrcGroup) == 0:
            return
        # トランクポートの時にどこのチームVLANに所属しているかを確認する
        SrcVlan = False
        if self.port_vlan[str(port)] is not False:
            SrcVlan = pkt.get_protocol(vlan.vlan)
            if SrcVlan:
                # print(SrcVlan.vid)
                # ここでグループが一つになっているはず
                SrcGroup = [i for i in self.group_vlan if SrcVlan in i]
        SrcGroup = SrcGroup[0]
        # Arp情報
        pkt_arp = pkt.get_protocol(arp.arp)
        if pkt_arp:
            # ARP情報があった場合、ARPリクエストだった場合はARPリクエストを返す
            self._handle_arp(datapath, port, pkt_ethernet, pkt_arp, SrcGroup)
            return
        # IP情報を取得する
        pkt_ipv4 = pkt.get_protocol(ipv4.ipv4)
        if pkt_ipv4:
            if "192.168." in pkt_ipv4.dst[:8]:
                return
            IpSeg = pkt_ipv4.dst.split('.')
            # 10.0.0.0/8へのアクセスを考える
            if IpSeg[0] == "10" and 1 <= int(IpSeg[1]) and int(IpSeg[1]) <= 15:
                self._handle_internal_nat(datapath, port, pkt, pkt_ethernet, pkt_ipv4, SrcGroup, SrcVlan)
            # 192.168.0.0/16へのアクセスを考える

            if IpSeg[0] == "10" and 1 > int(IpSeg[1]) and int(IpSeg[1]) > 15:
                self._handle_external_nat(datapath, port, pkt, pkt_ethernet, pkt_ipv4, SrcGroup, SrcVlan)
            # グローバルへのアクセスを考える
            self._handle_global_nat(datapath, port, pkt, pkt_ethernet, pkt_ipv4, SrcGroup, SrcVlan)

        pkt_icmp = pkt.get_protocol(icmp.icmp)
        if pkt_icmp:
            # ICMP情報があった場合、ICMPリクエストだった場合はICMPを返す
            self._handle_icmp(datapath, port, pkt_ethernet, pkt_ipv4, pkt_icmp, SrcGroup)
            return
